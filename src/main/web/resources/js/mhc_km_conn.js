
/* Maudlin Honey Cetyre copyright Nicholas Prado;
	released under Prosperity Public license terms */

var mhcConn = {
	channel : null,
	supported : 'WebSocket' in window,


	init : function()
	{
		if ( ! this.supported )
		{
			alert( 'Try a browser that supports websockets' );
		}
		else
		{
			this.channel = new WebSocket( 'ws://localhost:9998' );
			this.channel.onopen = function coo()
			{
				std.log( 'connected' );
			};
			this.channel.onclose = function coc()
			{
				std.log( 'session over' );
				alert( 'The server disconnected our session' );
			};
			this.channel.onmessage = function com( message )
			{
				std.log( 'heard '+ message );
			};
		}
	},


	sendKeymap : function( keymap )
	{
		var keymapMessage = {
				msgType : 'KEYMAP',
				keymap : keymap
		};
		this.channel.send( JSON.stringify( keymapMessage ) );
	},
};