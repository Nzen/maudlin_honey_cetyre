
/* Maudlin Honey Cetyre copyright Nicholas Prado;
	released under Prosperity Public license terms */

var mhcInput =
{
	sendKeymap : function()
	{
		var keymap = {
			moveLeft : document.getElementById( "tb_key_left" ).value,
			moveRight : document.getElementById( "tb_key_right" ).value,
			moveUp : document.getElementById( "tb_key_up" ).value,
			moveUpLeft : document.getElementById( "tb_key_upleft" ).value,
			moveUpRight : document.getElementById( "tb_key_upright" ).value,
			moveDownLeft : document.getElementById( "tb_key_downleft" ).value,
			moveDownRight : document.getElementById( "tb_key_downright" ).value,
			quitGame : document.getElementById( "tb_key_quit" ).value
		};
		mhcConn.sendKeymap( keymap );
	}
};






























