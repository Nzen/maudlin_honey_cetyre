/* see ../../../../../LICENSE for release details */
package ws.nzen.game.adventure.mhc.components;

import java.awt.Point;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rlforj.pathfinding.AStar;
import ws.nzen.game.adventure.mhc.Entity;
import ws.nzen.game.adventure.mhc.location.GameMap;

/**  */
public class Ai
{
	final Logger outChannel = LoggerFactory.getLogger( Ai.class );
	private Entity owner;


	public Entity getOwner()
	{
		return owner;
	}
	public void setOwner( Entity owner )
	{
		this.owner = owner;
	}


	/** @returns whether moved, as caller needs to rekey */
	public boolean takeTurn(
			Entity target, GameMap floor, Collection<Entity> others, AStar paths )
	{
		Point intended = target.getPosition();
		if ( owner.getPosition().distance( intended ) >= 2D )
		{
			return owner.moveTowards( intended, floor, others, paths );
		}
		else if ( target.getFighter() != null
				&& target.getFighter().getCurrHp() > 0 )
		{
			outChannel.info( owner.getName() +" attacks your self worth" );
		}
		return false;
	}

}





































