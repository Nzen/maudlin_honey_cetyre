package ws.nzen.game.adventure.mhc.message;

public class ClientJsonMessage
{
	public static final String FLAG_TYPE = "msgType";
	/** From client, indicating key movement */
	public static final String TYPE_KEY = "KEY";
	public static final String KEY_F_PRESSING = "pressing";
	public static final String KEY_F_KCODE = "code";
	/** For client, indicating what to draw */
	public static final String TYPE_CANVAS = "CANVAS";
	public static final String CANVAS_F_VALUE = "value";
	public static final String TYPE_KEYMAP = "KEYMAP";
	public static final String
			KEYMAP_F_KEYMAP = "keymap",
			KEYMAP_F_LEFT = "moveLeft",
			KEYMAP_F_RIGHT = "moveRight",
			KEYMAP_F_UP = "moveUp",
			KEYMAP_F_DOWN = "moveDown",
			KEYMAP_F_UP_LEFT = "moveUpLeft",
			KEYMAP_F_UP_RIGHT = "moveUpRight",
			KEYMAP_F_DOWN_LEFT = "moveDownLeft",
			KEYMAP_F_DOWN_RIGHT = "moveDownRight",
			KEYMAP_F_QUIT = "quitGame";

}


















