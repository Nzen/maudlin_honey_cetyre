/** see ../../../../../LICENSE for release details */
package ws.nzen.game.adventure.mhc;

import java.awt.Point;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rlforj.pathfinding.AStar;
import ws.nzen.game.adventure.mhc.components.Fighter;
import ws.nzen.game.adventure.mhc.location.GameMap;
import ws.nzen.game.adventure.mhc.message.Cell;
import ws.nzen.game.adventure.mhc.message.ClientJsonMessage;
import ws.nzen.game.adventure.mhc.message.Move;
import ws.nzen.game.adventure.mhc.message.Request;
import ws.nzen.game.adventure.mhc.message.Type;

/**  */
public class Engine extends WebSocketServer
{
	private static final Point maxScreenDim = new Point( 10, 10 );
	private static final Point maxMapDim = new Point( 30, 30 );
	private static final int roomMinSide = 4, roomMaxSide = 10;
	private static final int maxRooms = 10;
	private static final int fovRadius = 10;
	final Logger outChannel = LoggerFactory.getLogger( Engine.class );
	private GameMap board = new GameMap( maxMapDim );
	// private boolean shouldChangeFov = true;
	private AStar paths = new AStar( board, maxMapDim.x, maxMapDim.y, true );

	private GameState mode = GameState.AVATAR_TURN;

	private Entity playerAvatar = new Entity(
			new Point( maxScreenDim.x /2, maxScreenDim.y /2 ),
			"@", "silver", "avatar", true,
			new Fighter( 30, 2, 5 ), null );
	private Collection<Entity> entities = new LinkedList<>();

	private Keymap inputMap = new Keymap();

	/** @param args */
	public static void main( String[] args )
	{
		Engine toSplitApartLater;
		try
		{
			toSplitApartLater = new Engine();
			toSplitApartLater.start();
		}
		catch ( UnknownHostException uhe )
		{
			System.err.println( "Server couldn't start, "+ uhe );
		}
	}


	public Engine() throws UnknownHostException
	{
		super( new InetSocketAddress( 9998 ) );
	}


	@Override
	public void onStart()
	{
		outChannel.info( "Server started!" );
		setConnectionLostTimeout( 0 );
		setConnectionLostTimeout( 100 );
	}


	@Override
	public void onOpen( WebSocket conn, ClientHandshake handshake )
	{
		outChannel.info( "began with "+ conn.getRemoteSocketAddress()
				+" id-"+ conn.hashCode() );
		board.makeMap( maxRooms, roomMinSide, roomMaxSide, playerAvatar, entities, 3 );
		conn.send( boardViewMessage().toString() );
	}


	@Override
	public void onMessage( WebSocket conn, String jsonMsg )
	{
		// outChannel.info( "received "+ jsonMsg +" "+ conn.hashCode() );
		JSONObject mapped = new JSONObject( jsonMsg );
		if ( mapped.has( ClientJsonMessage.FLAG_TYPE ) )
		{
			if ( mapped.getString( ClientJsonMessage.FLAG_TYPE )
					.equals( ClientJsonMessage.TYPE_KEY )
					// IMPROVE store the keypress state, rather than only use keypress
					&& mapped.getBoolean( ClientJsonMessage.KEY_F_PRESSING ) )
			{
				Request move = HandlesInput.fromKey(
						mapped.getString( ClientJsonMessage.KEY_F_KCODE ),
						inputMap );
				if ( move.type() == Type.MOVE && mode == GameState.AVATAR_TURN )
				{
					boolean did = playerAvatar.move(
							(Move)move, board, entities, true );
					if ( did )
					{
						board.recalcFieldOfView( playerAvatar, fovRadius );
						mode = GameState.WORLD_TURN;
						conn.send( boardViewMessage().toString() );
					}
				}
				else if ( move.type() == Type.QUIT )
				{
					conn.close();
					outChannel.info( "told to quit" );
					System.exit( 0 );
				}
			}
			else if ( mapped.getString( ClientJsonMessage.FLAG_TYPE )
					.equals( ClientJsonMessage.TYPE_KEYMAP ) )
			{
				inputMap.adopt( translateKeymap(
						mapped.getJSONObject( ClientJsonMessage.KEYMAP_F_KEYMAP ) ) );
			}
		}
		if ( mode == GameState.WORLD_TURN )
		{
			for ( Entity who : entities )
			{
				if ( ! board.relevant( who.getPosition() ) )
					continue;
				if ( who.getAi() != null )
				{
					who.getAi().takeTurn(
							playerAvatar, board, entities, paths );
				}
			}
			mode = GameState.AVATAR_TURN;
		}
	}


	@Override
	public void onClose( WebSocket conn, int code, String reason, boolean remote )
	{
		outChannel.info( "finished with "+ conn
				.getRemoteSocketAddress().getAddress().getHostAddress() );
	}


	@Override
	public void onError( WebSocket conn, Exception ex )
	{
		String connId = ( conn != null  )
				? conn.getRemoteSocketAddress().getAddress().toString() : "null socket";
		outChannel.info( "failed with "+ connId +"\n"+ ex );
	}


	private JSONObject boardViewMessage()
	{
		JSONObject wholeMessage = new JSONObject();
		// this'll be clumsy until I switch to gson
		JSONArray layers = new JSONArray();
		for ( List<Cell> layer : board.boardForClient( playerAvatar, entities ) )
		{
			JSONArray currLayer = new JSONArray();
			for ( Cell cell : layer )
			{
				JSONObject jsCell = new JSONObject();
				jsCell.put( "symbol", cell.getSymbol() );
				jsCell.put( "color", cell.getColor() );
				jsCell.put( "xxPos", cell.getXxPos() );
				jsCell.put( "yyPos", cell.getYyPos() );
				currLayer.put( jsCell );
			}
			layers.put( currLayer );
		}
		wholeMessage.put( ClientJsonMessage.FLAG_TYPE, ClientJsonMessage.TYPE_CANVAS );
		wholeMessage.put( ClientJsonMessage.CANVAS_F_VALUE, layers );
		return wholeMessage;
	}


	private Map<String, String> translateKeymap( JSONObject jsKeymap )
	{
		Map<String, String> jKeymap = new HashMap<>();
		// this'll be clumsy until I switch to gson
		if ( jsKeymap.has( ClientJsonMessage.KEYMAP_F_LEFT ) )
		{
			jKeymap.put(
					ClientJsonMessage.KEYMAP_F_LEFT,
					jsKeymap.getString( ClientJsonMessage.KEYMAP_F_LEFT ) );
		}
		if ( jsKeymap.has( ClientJsonMessage.KEYMAP_F_RIGHT ) )
		{
			jKeymap.put(
					ClientJsonMessage.KEYMAP_F_RIGHT,
					jsKeymap.getString( ClientJsonMessage.KEYMAP_F_RIGHT ) );
		}
		if ( jsKeymap.has( ClientJsonMessage.KEYMAP_F_UP ) )
		{
			jKeymap.put(
					ClientJsonMessage.KEYMAP_F_UP,
					jsKeymap.getString( ClientJsonMessage.KEYMAP_F_UP ) );
		}
		if ( jsKeymap.has( ClientJsonMessage.KEYMAP_F_DOWN ) )
		{
			jKeymap.put(
					ClientJsonMessage.KEYMAP_F_DOWN,
					jsKeymap.getString( ClientJsonMessage.KEYMAP_F_DOWN ) );
		}
		if ( jsKeymap.has( ClientJsonMessage.KEYMAP_F_QUIT ) )
		{
			jKeymap.put(
					ClientJsonMessage.KEYMAP_F_QUIT,
					jsKeymap.getString( ClientJsonMessage.KEYMAP_F_QUIT ) );
		}
		return jKeymap;
	}

}
























































