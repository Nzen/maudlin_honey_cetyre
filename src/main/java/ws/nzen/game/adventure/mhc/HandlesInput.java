/** see ../../../../../LICENSE for release details */
package ws.nzen.game.adventure.mhc;

import ws.nzen.game.adventure.mhc.message.Move;
import ws.nzen.game.adventure.mhc.message.Quit;
import ws.nzen.game.adventure.mhc.message.Request;

/**  */
public class HandlesInput
{

	public static Request fromKey( String keycode, Keymap keymap )
	{
		if ( keymap.getLeftKey().equals( keycode ) )
		{
			return new Move( -1, 0 );
		}
		else if ( keymap.getRightKey().equals( keycode ) )
		{
			return new Move( 1, 0 );
		}
		else if ( keymap.getUpKey().equals( keycode ) )
		{
			return new Move( 0, -1 );
		}
		else if ( keymap.getDownKey().equals( keycode ) )
		{
			return new Move( 0, 1 );
		}
		else if ( keymap.getUpLeftKey().equals( keycode ) )
		{
			return new Move( -1, -1 );
		}
		else if ( keymap.getUpRightKey().equals( keycode ) )
		{
			return new Move( 1, -1 );
		}
		else if ( keymap.getDownLeftKey().equals( keycode ) )
		{
			return new Move( -1, 1 );
		}
		else if ( keymap.getDownRightKey().equals( keycode ) )
		{
			return new Move( 1, 1 );
		}
		else if ( keymap.getQuitKey().equals( keycode ) )
		{
			return new Quit();
		}
		else
		{
			throw new RuntimeException( "unhandled message type killed me" );
		}
	}

}


















