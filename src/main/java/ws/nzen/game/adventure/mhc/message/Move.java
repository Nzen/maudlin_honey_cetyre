/** see ../../../../../LICENSE for release details */
package ws.nzen.game.adventure.mhc.message;

import java.awt.Point;

/**  */
public class Move implements Request
{
	private Point where;


	public Move( int xx, int yy )
	{
		where = new Point( xx, yy );
	}


	public Move( Point become )
	{
		where = become;
	}


	/* (non-Javadoc)
	 * @see ws.nzen.game.adventure.mhc.ClientRequest#type()
	 */
	@Override
	public Type type()
	{
		return Type.MOVE;
	}


	public Point getWhere()
	{
		return where;
	}

}


















