/** see ../../../../../LICENSE for release details */
package ws.nzen.game.adventure.mhc;

import java.awt.Point;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rlforj.pathfinding.AStar;
import ws.nzen.game.adventure.mhc.components.Ai;
import ws.nzen.game.adventure.mhc.components.Fighter;
import ws.nzen.game.adventure.mhc.location.GameMap;
import ws.nzen.game.adventure.mhc.message.Move;

/**  */
public class Entity
{
	final Logger outChannel = LoggerFactory.getLogger( Entity.class );
	private Point position;
	private String symbol;
	private String color;
	private String name;
	private boolean blocks = false;
	private Fighter fighter = null;
	private Ai ai = null;


	public Entity(
			Point where, String represents, String shade,
			String called, boolean resistant,
			Fighter attackNature, Ai goalNature )
	{
		position = where;
		symbol = represents;
		color = shade;
		name = called;
		blocks = resistant;
		fighter = attackNature;
		ai = goalNature;

		if ( fighter != null )
			fighter.setOwner( this );
		if ( ai != null )
			ai.setOwner( this );
	}


	public boolean move(
			Move update, GameMap floor, Collection<Entity> others,
			boolean amAvatar )
	{
		Point intended = movedPosition( update );
		return move( intended, floor, others, amAvatar );
	}


	public boolean move(
			Point intended, GameMap floor, Collection<Entity> others,
			boolean amAvatar )
	{
		if ( ! floor.impassibleAt( intended ) )
		{
			Entity schrodinger = implacablyOccupied( intended, others );
			if ( schrodinger == null )
			{
				position.setLocation( intended );
				return true;
			}
			else if ( amAvatar )
			{
				outChannel.info( "kicked "+ schrodinger.getName() );
				return true;
			}
			// ASK am I double attacking when enemies use this ? they probably won't move if in range
		}
		return false;
	}


	public boolean moveTowards(
			Point target, GameMap board, Collection<Entity> others )
	{
		int dxx = target.x - position.x;
		int dyy = target.y - position.y;
		double distance = Math.sqrt( Math.pow( dxx, 2 ) + Math.pow( dyy, 2 ) );
		int nextXx = (int)Math.round( dxx / distance );
		int nextYy = (int)Math.round( dyy / distance );
		return move( new Move( nextXx, nextYy ), board, others, false );
	}


	public boolean moveTowards(
			Point target, GameMap board, Collection<Entity> others, AStar paths )
	{
		rlforj.math.Point[] stepsToTake = paths.findPath(
				getXxPos(), getYyPos(), target.x, target.y, 10 );
		int stepPastCurrentPositionInd = 1;
		if ( stepsToTake != null )
		{
			return move( new Point(
						stepsToTake[ stepPastCurrentPositionInd ].x,
						stepsToTake[ stepPastCurrentPositionInd ].y ),
					board, others, false );
		}
		return false;
	}


	private Point movedPosition( Move update )
	{
		return new Point(
				getXxPos() + update.getWhere().x,
				getYyPos() + update.getWhere().y );
	}


	// NOTE here for tutorial compatibility, I'd prefer this in GameMap
	private Entity implacablyOccupied( Point where, Collection<Entity> others )
	{
		for ( Entity who : others )
		{
			if ( who.getXxPos() == where.x
					&& who.getYyPos() == where.y
					&& who.blocks() )
			{
				return who;
			}
		}
		return null;
	}


	public void setPosition( Point directly )
	{
		position = directly;
	}
	public Point getPosition()
	{
		return position;
	}


	public int getXxPos()
	{
		return position.x;
	}
	public void setXxPos( int xxPos )
	{
		position.setLocation( xxPos, position.y );
	}


	public int getYyPos()
	{
		return position.y;
	}
	public void setYyPos( int yyPos )
	{
		position.setLocation( position.x, yyPos );
	}


	public String getSymbol()
	{
		return symbol;
	}
	public void setSymbol( String symbol )
	{
		this.symbol = symbol;
	}


	public String getColor()
	{
		return color;
	}
	public void setColor( String color )
	{
		this.color = color;
	}


	public String getName()
	{
		return name;
	}
	public void setName( String name )
	{
		this.name = name;
	}


	public boolean blocks()
	{
		return blocks;
	}
	public void setBlocks( boolean implacible )
	{
		this.blocks = implacible;
	}


	public Fighter getFighter()
	{
		return fighter;
	}


	public Ai getAi()
	{
		return ai;
	}

}


















