/* see ../../../../../LICENSE for release details */
package ws.nzen.game.adventure.mhc;

import java.util.Map;

import ws.nzen.game.adventure.mhc.message.ClientJsonMessage;

/**  */
public class Keymap
{
	private String leftKey = "s";
	private String rightKey = "f";
	private String upKey = "e";
	private String downKey = "d";
	private String upLeftKey = "w";
	private String upRightKey = "r";
	private String downLeftKey = "x";
	private String downRightKey = "v";
	private String quitKey = "p";


	public void adopt( Map<String, String> jsKeymap )
	{
		if ( jsKeymap.containsKey( ClientJsonMessage.KEYMAP_F_LEFT ) )
		{
			leftKey = jsKeymap.get( ClientJsonMessage.KEYMAP_F_LEFT );
		}
		if ( jsKeymap.containsKey( ClientJsonMessage.KEYMAP_F_RIGHT ) )
		{
			rightKey = jsKeymap.get( ClientJsonMessage.KEYMAP_F_RIGHT );
		}
		if ( jsKeymap.containsKey( ClientJsonMessage.KEYMAP_F_UP ) )
		{
			upKey = jsKeymap.get( ClientJsonMessage.KEYMAP_F_UP );
		}
		if ( jsKeymap.containsKey( ClientJsonMessage.KEYMAP_F_DOWN ) )
		{
			downKey = jsKeymap.get( ClientJsonMessage.KEYMAP_F_DOWN );
		}

		if ( jsKeymap.containsKey( ClientJsonMessage.KEYMAP_F_UP_LEFT ) )
		{
			upKey = jsKeymap.get( ClientJsonMessage.KEYMAP_F_UP_LEFT );
		}
		if ( jsKeymap.containsKey( ClientJsonMessage.KEYMAP_F_UP_RIGHT ) )
		{
			upKey = jsKeymap.get( ClientJsonMessage.KEYMAP_F_UP_RIGHT );
		}
		if ( jsKeymap.containsKey( ClientJsonMessage.KEYMAP_F_DOWN_LEFT ) )
		{
			downKey = jsKeymap.get( ClientJsonMessage.KEYMAP_F_DOWN_LEFT );
		}
		if ( jsKeymap.containsKey( ClientJsonMessage.KEYMAP_F_DOWN_RIGHT ) )
		{
			downKey = jsKeymap.get( ClientJsonMessage.KEYMAP_F_DOWN_RIGHT );
		}
		if ( jsKeymap.containsKey( ClientJsonMessage.KEYMAP_F_QUIT ) )
		{
			quitKey = jsKeymap.get( ClientJsonMessage.KEYMAP_F_QUIT );
		}
	}


	public String getLeftKey()
	{
		return leftKey;
	}
	public void setLeftKey( String leftKey )
	{
		this.leftKey = leftKey;
	}


	public String getRightKey()
	{
		return rightKey;
	}
	public void setRightKey( String rightKey )
	{
		this.rightKey = rightKey;
	}


	public String getUpKey()
	{
		return upKey;
	}
	public void setUpKey( String upKey )
	{
		this.upKey = upKey;
	}


	public String getDownKey()
	{
		return downKey;
	}
	public void setDownKey( String downKey )
	{
		this.downKey = downKey;
	}


	public String getUpLeftKey()
	{
		return upLeftKey;
	}
	public void setUpLeftKey( String upLeftKey )
	{
		this.upLeftKey = upLeftKey;
	}


	public String getUpRightKey()
	{
		return upRightKey;
	}
	public void setUpRightKey( String upRightKey )
	{
		this.upRightKey = upRightKey;
	}


	public String getDownLeftKey()
	{
		return downLeftKey;
	}
	public void setDownLeftKey( String downLeftKey )
	{
		this.downLeftKey = downLeftKey;
	}


	public String getDownRightKey()
	{
		return downRightKey;
	}
	public void setDownRightKey( String downRightKey )
	{
		this.downRightKey = downRightKey;
	}


	public String getQuitKey()
	{
		return quitKey;
	}
	public void setQuitKey( String quitKey )
	{
		this.quitKey = quitKey;
	}


	

}


















