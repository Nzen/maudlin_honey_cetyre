/* see ../../../../../LICENSE for release details */
package ws.nzen.game.adventure.mhc.location;

/**  */
public class Tile
{
	private boolean impassable;
	private boolean occluded;
	private boolean experienced = false;


	public Tile( boolean blocksMovement )
	{
		impassable = blocksMovement;
		occluded = blocksSight();
	}


	public boolean blocksSight()
	{
		return impassable;
	}


	public boolean isImpassable()
	{
		return impassable;
	}
	public void setImpassable( boolean impassable )
	{
		this.impassable = impassable;
	}


	public boolean isOccluded()
	{
		return occluded;
	}
	public void setOccluded( boolean occluded )
	{
		this.occluded = occluded;
	}


	public boolean isExperienced()
	{
		return experienced;
	}
	public void setExperienced( boolean experienced )
	{
		this.experienced = experienced;
	}

}


















