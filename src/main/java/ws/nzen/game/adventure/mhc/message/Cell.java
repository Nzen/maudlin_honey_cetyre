/** see ../../../../../LICENSE for release details */
package ws.nzen.game.adventure.mhc.message;

/**  */
public class Cell
{
	private String symbol;
	private String color = "blue";
	private int xxPos;
	private int yyPos;


	public Cell( String show, int xx, int yy, String color )
	{
		setSymbol( show );
		setXxPos( xx );
		setYyPos( yy );
		setColor( color );
	}


	public void setSymbol( String symbol )
	{
		this.symbol = symbol;
	}
	public void setColor( String color )
	{
		// consider a set of valid values ?
		this.color = color;
	}
	public void setXxPos( int xxPos )
	{
		if ( xxPos < 0 )
			xxPos = 0;
		// awareness of max ?
		this.xxPos = xxPos;
	}
	public void setYyPos( int yyPos )
	{
		if ( yyPos < 0 )
			yyPos = 0;
		// awareness of max ?
		this.yyPos = yyPos;
	}


	public String getSymbol()
	{
		return symbol;
	}
	public String getColor()
	{
		return color;
	}
	public int getXxPos()
	{
		return xxPos;
	}
	public int getYyPos()
	{
		return yyPos;
	}

}


















