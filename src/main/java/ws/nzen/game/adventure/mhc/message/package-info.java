/* see ../../../../../LICENSE for release details */
/** Has classes related to the representing message content. */
package ws.nzen.game.adventure.mhc.message;