/* see ../../../../../LICENSE for release details */
package ws.nzen.game.adventure.mhc.location;

import java.awt.Point;

/**  */
public class Rectangle
{
	private Point upperLeft;
	private Point lowerRight;


	public Rectangle( Point topL, Point downR )
	{
		upperLeft = topL;
		lowerRight = downR;
	}


	public Rectangle( int topXx, int topYy, int bottomXx, int bottomYy )
	{
		this(
				new Point( topXx, topYy ),
				new Point( bottomXx, bottomYy ) );
	}


	// If not for the tutorial, I'd prefer using Awt.Rectangle, if only for intersection()
	public Point center()
	{
		int centerXx = ( upperLeft.x + lowerRight.x ) /2;
		int centerYy = ( upperLeft.y + lowerRight.y ) /2;
		return new Point( centerXx, centerYy );
	}


	public boolean intersectsWith( Rectangle another )
	{
		return upperLeft.x <= another.upperLeft.x
				&& upperLeft.x + lowerRight.x >= upperLeft.x + another.lowerRight.x
				&& upperLeft.y <= another.upperLeft.y
				&& upperLeft.y + lowerRight.y >= upperLeft.y + another.lowerRight.y;
	}


	public int width()
	{
		return lowerRight.x - upperLeft.x;
	}


	public int height()
	{
		return lowerRight.y - upperLeft.y;
	}


	public Point getUpperLeft()
	{
		return upperLeft;
	}


	public void setUpperLeft( Point upperLeft )
	{
		this.upperLeft = upperLeft;
	}


	public Point getLowerRight()
	{
		return lowerRight;
	}


	public void setLowerRight( Point lowerRight )
	{
		this.lowerRight = lowerRight;
	}


	@Override
	public String toString()
	{
		StringBuilder bla = new StringBuilder();
		bla.append( "(" );
		bla.append( upperLeft.x );
		bla.append( "," );
		bla.append( upperLeft.y );
		bla.append( ") (" );
		bla.append( lowerRight.x );
		bla.append( "," );
		bla.append( lowerRight.y );
		bla.append( ")" );
		return bla.toString();
	}

}

































