/** see ../../../../../LICENSE for release details */
package ws.nzen.game.adventure.mhc.message;

/**  */
public class Quit implements Request
{

	/* (non-Javadoc)
	 * @see ws.nzen.game.adventure.mhc.message.Request#type()
	 */
	@Override
	public Type type()
	{
		return Type.QUIT;
	}

}


















