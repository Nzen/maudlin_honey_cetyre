/* see ../../../../../LICENSE for release details */
package ws.nzen.game.adventure.mhc.components;

import ws.nzen.game.adventure.mhc.Entity;

/**  */
public class Fighter
{

	private int maxHp;
	private int currHp;
	private int defense;
	private int power;
	private Entity owner;


	public Fighter( int hp, int def, int attack )
	{
		maxHp = hp;
		currHp = hp;
		defense = def;
		power = attack;
	}


	public int getMaxHp()
	{
		return maxHp;
	}
	public void setMaxHp( int maxHp )
	{
		this.maxHp = maxHp;
	}


	public int getCurrHp()
	{
		return currHp;
	}
	public void setCurrHp( int currHp )
	{
		this.currHp = currHp;
	}


	public int getDefense()
	{
		return defense;
	}
	public void setDefense( int defense )
	{
		this.defense = defense;
	}


	public int getPower()
	{
		return power;
	}
	public void setPower( int power )
	{
		this.power = power;
	}


	public Entity getOwner()
	{
		return owner;
	}
	public void setOwner( Entity owner )
	{
		this.owner = owner;
	}

}


















