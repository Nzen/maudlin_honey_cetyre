/** see ../../../../../LICENSE for release details */
package ws.nzen.game.adventure.mhc.location;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import rlforj.IBoard;
import rlforj.los.IFovAlgorithm;
import rlforj.los.ShadowCasting;
import ws.nzen.game.adventure.mhc.Entity;
import ws.nzen.game.adventure.mhc.components.Ai;
import ws.nzen.game.adventure.mhc.components.Fighter;
import ws.nzen.game.adventure.mhc.message.Cell;

/**  */
public class GameMap implements IBoard
{
	private Point maxMapDim;
	private Map<Point, Tile> tiles;
	private Map<Point, Boolean> currentlyRelevant = new HashMap<>();
	private IFovAlgorithm sightMonster =  new ShadowCasting();


	public GameMap( 
			Point lowerRight )
	{
		maxMapDim = lowerRight;
		tiles = initializeTiles();
	}


	private Map<Point, Tile> initializeTiles()
	{
		Map<Point, Tile> terrain = new HashMap<>();
		for ( int xx = maxMapDim.x -1; xx >= 0; xx-- )
		{
			for ( int yy = maxMapDim.y -1; yy >= 0; yy-- )
			{
				terrain.put( new Point( xx, yy ), new Tile( true ) );
			}
		}
		return terrain;
	}


	public void makeMap(
			int maxRooms, int roomMinSide, int roomMaxSide,
			Entity toCarveAround, Collection<Entity> entities,
			int maxEnemiesPerRoom )
	{
		final int roomSideDiff = roomMaxSide - roomMinSide;
		final int maxTopLeftPosXx = maxMapDim.x - roomMinSide,
				maxTopLeftPosYy = maxMapDim.y - roomMinSide;
		final int minRooms = 2;
		Collection<Rectangle> rooms = new LinkedList<>();
		Point previousRoomCenter = null;
		int roomsMade = 0;
		Random oracle = new Random();
		for ( int roomInd = maxRooms;
				roomInd > 0; roomInd-- )
		{
			int roomSideXx = oracle.nextInt( roomSideDiff ) + roomMinSide;
			int roomSideYy = oracle.nextInt( roomSideDiff ) + roomMinSide;
			int tlPosYy = oracle.nextInt( maxTopLeftPosXx );
			int tlPosXx = oracle.nextInt( maxTopLeftPosYy );
			Rectangle room = new Rectangle(
					tlPosXx, tlPosYy,
					tlPosXx + roomSideXx, tlPosYy + roomSideYy );
			boolean isolated = true;
			for ( Rectangle valid : rooms )
			{
				if ( valid.intersectsWith( room ) )
				{
					isolated = false;
					break;
				}
			}
			if ( ! isolated && roomsMade > minRooms )
				break; // NOTE good enough
			else if ( ! isolated )
				continue; // NOTE try another dimension
			createRoom( room );
			placeEntities( room, entities, maxEnemiesPerRoom, oracle );
			Point center = room.center();
			if ( roomsMade == 0 )
			{
				toCarveAround.setPosition( center );
				entities.add( toCarveAround );
			}
			else
			{
				if ( oracle.nextBoolean() )
				{
					createHorizontalTunnel(
							previousRoomCenter.x,
							center.x,
							previousRoomCenter.y );
					createVerticalTunnel(
							center.x,
							previousRoomCenter.y,
							center.y );
				}
				else
				{
					createVerticalTunnel(
							center.x,
							previousRoomCenter.y,
							center.y );
					createHorizontalTunnel(
							previousRoomCenter.x,
							center.x,
							previousRoomCenter.y );
				}
				
			}
			previousRoomCenter = center;
			roomsMade++;
			rooms.add( room );
		}
		recalcFieldOfView( toCarveAround, roomMaxSide );
	}


	// perhaps public ?
	private void createRoom( Rectangle room )
	{
		Point cursor = new Point( 0, 0 );
		for ( int xxInd = room.getUpperLeft().x +1;
				xxInd < room.getLowerRight().x;
				xxInd++ )
		{
			for ( int yyInd = room.getUpperLeft().y +1;
					yyInd < room.getLowerRight().y;
					yyInd++ )
			{
				cursor.setLocation( xxInd, yyInd );
				if ( ! tiles.containsKey( cursor ) )
					continue;
				Tile cell = tiles.get( cursor );
				cell.setImpassable( false );
				cell.setOccluded( false );
			}
		}
	}


	private void createHorizontalTunnel(
			int startXx, int endXx, int rowYy )
	{
		Point cursor = new Point( 0, 0 );
		for ( int xxInd = Math.min( startXx, endXx );
				xxInd < Math.max( startXx, endXx ) +1;
				xxInd++ )
		{
			cursor.setLocation( xxInd, rowYy );
			if ( ! tiles.containsKey( cursor ) )
				continue;
			Tile cell = tiles.get( cursor );
			cell.setImpassable( false );
			cell.setOccluded( false );
		}
	}


	private void createVerticalTunnel(
			int columnXx, int startYy, int endYy )
	{
		Point cursor = new Point( 0, 0 );
		for ( int yyInd = Math.min( startYy, endYy );
				yyInd < Math.max( startYy, endYy ) +1;
				yyInd++ )
		{
			cursor.setLocation( columnXx, yyInd );
			if ( ! tiles.containsKey( cursor ) )
				continue;
			Tile cell = tiles.get( cursor );
			cell.setImpassable( false );
			cell.setOccluded( false );
		}
	}


	private void placeEntities(
			Rectangle room, Collection<Entity> entities,
			int maxEnemies, Random oracle )
	{
		int enemiesHere = oracle.nextInt( maxEnemies -1 ) +1;
		Ai goalNature = new Ai();
		for ( int enemInd = enemiesHere; enemInd > 0; enemInd-- )
		{
			int enemXx = oracle.nextInt( room.width() ) + room.getUpperLeft().x;
			int enemYy = oracle.nextInt( room.height() ) + room.getUpperLeft().y;
			Point where = new Point( enemXx, enemYy );
			boolean occupied = false;
			for ( Entity another : entities )
			{
				if ( another.getXxPos() == where.x
						&& another.getYyPos() == where.y )
				{
					occupied = true;
					break;
				}
			}
			if ( ! occupied )
			{
				Entity enemy;
				if ( oracle.nextInt( 10 ) > 8 )
					enemy = new Entity( new Point( where ),
							"o", "LightGreen", "orc", true,
							new Fighter( 10, 0, 3 ), goalNature );
				else
					enemy = new Entity( new Point( where ),
							"T", "DarkGreen", "troll", true,
							new Fighter( 16, 1, 4 ), goalNature );
				entities.add( enemy );
			}
		}
	}


	public void recalcFieldOfView(
			Entity avatar, int radius )
	{
		currentlyRelevant.clear();
		sightMonster.visitFoV(
				this,
				avatar.getXxPos(),
				avatar.getYyPos(),
				radius );
	}


	public List<List<Cell>> boardForClient(
			Entity avatar, Collection<Entity> entities )
	{
		List<List<Cell>> everyLayer = new LinkedList<>();
		List<Cell> onlyLayer = new ArrayList<>();
		String colorDarkWall = "DarkBrown", colorDarkness = "DarkBlue",
				colorLitWall = "Chocolate", colorLightness = "DeepSkyBlue";
		String wallChar = "#", dirtChar = "`";
		// NOTE show currently visible area
		for ( Point where : currentlyRelevant.keySet() )
		{
			Cell toDraw = null;
			if ( where.equals( avatar.getPosition() ) )
			{
				Entity who = avatar;
				toDraw = new Cell( 
						who.getSymbol(), who.getXxPos(),
						who.getYyPos(), who.getColor() );
			}
			else
			{
				boolean occupied = false;
				for ( Entity who : entities )
				{
					if ( who.getXxPos() == where.x
							&& who.getYyPos() == where.y )
					{
						occupied = true;
						toDraw = new Cell( 
								who.getSymbol(), who.getXxPos(),
								who.getYyPos(), who.getColor() );
						break;
					}
				}
				if ( ! occupied )
				{
					Tile what = tiles.get( where );
					String solid = what.isImpassable() ? wallChar : dirtChar;
					String tint = what.isImpassable() ? colorLitWall : colorLightness;
					toDraw = new Cell( solid, where.x, where.y, tint );
				}
			}
			tiles.get( where ).setExperienced( true );
			onlyLayer.add( toDraw );
		}
		// NOTE show places we've been as dark memories
		for ( Point where : tiles.keySet() )
		{
			if ( ! currentlyRelevant.containsKey( where ) )
			{
				Tile what = tiles.get( where );
				if ( ! what.isExperienced() )
					continue;
				String solid = what.isImpassable() ? wallChar : dirtChar;
				String tint = what.isImpassable() ? colorDarkWall : colorDarkness;
				Cell toDraw = new Cell( solid, where.x, where.y, tint );
				onlyLayer.add( toDraw );
			}
		}
		everyLayer.add( onlyLayer );
		return everyLayer;
	}


	public boolean contains( Point where )
	{
		return tiles.containsKey( where );
	}


	public boolean impassibleAt( Point where )
	{
		return contains( where )
				&& tiles.get( where ).isImpassable();
	}


	public boolean occludedAt( Point where )
	{
		return contains( where )
				&& tiles.get( where ).isOccluded();
	}


	@Override
	public boolean contains( int xx, int yy )
	{
		return contains( new Point( xx, yy ) );
	}


	@Override
	public boolean blocksLight( int xx, int yy )
	{
		return occludedAt( new Point( xx, yy ) );
	}


	@Override
	public boolean blocksStep( int xx, int yy )
	{
		return impassibleAt( new Point( xx, yy ) );
	}


	@Override
	public void visit( int xx, int yy )
	{
		Point affirmed = new Point( xx, yy );
		currentlyRelevant.put( affirmed, Boolean.valueOf( true ) );
	}


	public void reset()
	{
		currentlyRelevant.clear();
	}


	public boolean relevant( Point intended )
	{
		return currentlyRelevant.containsKey( intended );
	}

}
























































